export class User {
  username: string;
  password: string;
  isLoggedIn: boolean;

  constructor(username: string, password: string) {
    this.username = username;
    this.password = password;
    this.isLoggedIn = false;
  }
}
