import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../model/user';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';

import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  USER_NAME_SESSION_ATTRIBUTE_NAME = 'authenticatedUser';
  USER_PASSWORD_SESSION_ATTIBUTE_NAME = 'authenticatedPassword';
  private loginEndpoint: string;

  private loggedInUser: User;

  constructor(private http: HttpClient, private router: Router) {
    this.loginEndpoint = environment.apiUrl + '/auth';
  }

  getBasicAuthTokenHeader(): object {
    this.loggedInUser = this.getLoggedInUserName();
    if (!this.loggedInUser) {
      this.logout();
    }
    return { headers: { authorization: this.createBasicAuthToken(this.loggedInUser) } };
  }

  authenticationService(user: User) {
    return this.http.get(this.loginEndpoint,
      { headers: { authorization: this.createBasicAuthToken(user) } }).pipe(
        map((res) => {
          this.loggedInUser = user;
          this.registerSuccessfulLogin(user);
        }));
  }

  createBasicAuthToken(user: User): string {
    return 'Basic ' + window.btoa(user.username + ':' + user.password);
  }

  registerSuccessfulLogin(user: User): void {
    sessionStorage.setItem(this.USER_NAME_SESSION_ATTRIBUTE_NAME, user.username);
    sessionStorage.setItem(this.USER_PASSWORD_SESSION_ATTIBUTE_NAME, user.password);
  }

  logout(): void {
    sessionStorage.removeItem(this.USER_NAME_SESSION_ATTRIBUTE_NAME);
    sessionStorage.removeItem(this.USER_PASSWORD_SESSION_ATTIBUTE_NAME);
    this.loggedInUser = null;
    this.router.navigate(['/']);
  }

  isUserLoggedIn(): boolean {
    const user = sessionStorage.getItem(this.USER_NAME_SESSION_ATTRIBUTE_NAME);
    if (user === null) {
      return false;
    }
    return true;
  }

  getLoggedInUserName(): User {
    const username = sessionStorage.getItem(this.USER_NAME_SESSION_ATTRIBUTE_NAME);
    const password = sessionStorage.getItem(this.USER_PASSWORD_SESSION_ATTIBUTE_NAME);
    if (!username || !password) {
      return null;
    }
    return new User(username, password);
  }
}
