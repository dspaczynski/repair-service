import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './component/dashboard/dashboard.component';
import { DeviceComponent } from './component/device/device.component';
import { DeviceCategoryComponent } from './component/device-category/device-category.component';

const routes: Routes = [
  { path: 'device-category', component: DeviceCategoryComponent },
  { path: 'device', component: DeviceComponent },
  { path: '', component: DashboardComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RepairServiceRoutingModule { }
