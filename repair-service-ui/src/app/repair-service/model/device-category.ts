export interface DeviceCategoryAttrs {
  id: number;
  name: string;
}
export class DeviceCategory {
  id: number;
  name: string;

  constructor(attrs: Partial<DeviceCategoryAttrs> = {}) {
    this.id = attrs.id;
    this.name = attrs.name;
  }
}
