export interface DeviceAttrs {
  id: number;
  deviceCategoryId: number;
  name: string;
  status: string;
  parameters: object;
}

export class Device {
  id: number;
  deviceCategoryId: number;
  name: string;
  status: string;
  parameters: object;

  constructor(attrs: Partial<DeviceAttrs> = {}) {
    this.id = attrs.id;
    this.deviceCategoryId = attrs.deviceCategoryId;
    this.name = attrs.name;
    this.status = attrs.status;
    this.parameters = attrs.parameters;
  }
}
