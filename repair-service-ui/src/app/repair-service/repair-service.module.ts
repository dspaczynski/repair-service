import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RepairServiceRoutingModule } from './repair-service-routing.module';
import { DashboardComponent } from './component/dashboard/dashboard.component';
import { DeviceComponent } from './component/device/device.component';
import { ShareModule } from '../share/share.module';
import { DeviceCategoryComponent } from './component/device-category/device-category.component';
import { EditDeviceCategoryModalComponent } from './component/edit-device-category-modal/edit-device-category-modal.component';
import { MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material/dialog';

@NgModule({
  declarations: [DashboardComponent, DeviceComponent, DeviceCategoryComponent, EditDeviceCategoryModalComponent],
  imports: [
    CommonModule,
    ShareModule,
    RepairServiceRoutingModule
  ],
  providers: [
    { provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: { hasBackdrop: true } }
  ],
  entryComponents: [
    EditDeviceCategoryModalComponent
  ]
})
export class RepairServiceModule { }
