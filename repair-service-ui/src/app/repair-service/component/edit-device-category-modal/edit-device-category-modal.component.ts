import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DeviceCategory, DeviceCategoryAttrs } from '../../model/device-category';

@Component({
  selector: 'app-edit-device-category-modal',
  templateUrl: './edit-device-category-modal.component.html',
  styleUrls: ['./edit-device-category-modal.component.scss']
})
export class EditDeviceCategoryModalComponent implements OnInit {
  public static MODAL_WIDTH = '20em';
  isEdit = true;

  constructor(public dialogRef: MatDialogRef<EditDeviceCategoryModalComponent>,
    @Inject(MAT_DIALOG_DATA) public deviceCategory: DeviceCategory) { }

  ngOnInit() {
    if (!this.deviceCategory) {
      this.isEdit = false;
      this.deviceCategory = new DeviceCategory();
    }
  }

  submitFrom(deviceCategoryAttrs: DeviceCategoryAttrs) {
    this.dialogRef.close(deviceCategoryAttrs);
  }

  closeForm() {
    this.dialogRef.close();
  }

}
