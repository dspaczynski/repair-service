import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditDeviceCategoryModalComponent } from './edit-device-category-modal.component';

describe('EditDeviceCategoryModalComponent', () => {
  let component: EditDeviceCategoryModalComponent;
  let fixture: ComponentFixture<EditDeviceCategoryModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditDeviceCategoryModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditDeviceCategoryModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
