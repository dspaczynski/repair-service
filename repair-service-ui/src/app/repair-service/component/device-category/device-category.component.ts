import { Component, OnInit } from '@angular/core';
import { DeviceCategoryService } from '../../service/device-category.service';
import { DeviceCategory, DeviceCategoryAttrs } from '../../model/device-category';
import { MatDialog } from '@angular/material/dialog';
import { EditDeviceCategoryModalComponent } from '../edit-device-category-modal/edit-device-category-modal.component';
import { MatTable } from '@angular/material/table';

@Component({
  selector: 'app-device-category',
  templateUrl: './device-category.component.html',
  styleUrls: ['./device-category.component.scss']
})
export class DeviceCategoryComponent implements OnInit {
  displayedColumns: string[] = ['id', 'name', 'actions'];
  deviceCategories: DeviceCategory[];

  constructor(private deviceCategoryService: DeviceCategoryService, private dialog: MatDialog) { }

  ngOnInit() {
    this.deviceCategoryService.getDeviceCategories().subscribe(
      (deviceCategories: DeviceCategory[]) => this.deviceCategories = deviceCategories
    );
  }

  openAddDeviceCategoryModal(deviceCategoriesTable: MatTable<DeviceCategory>) {
    const dialogRef = this.dialog.open(EditDeviceCategoryModalComponent,
      {
        width: EditDeviceCategoryModalComponent.MODAL_WIDTH,
        data: null
      }
    );
    dialogRef.afterClosed().subscribe((deviceCategoryAttrs: DeviceCategoryAttrs) =>
      this.deviceCategoryService.addDeviceCategory(deviceCategoryAttrs).subscribe(
        (newDeviceCategory: DeviceCategory) => {
          this.deviceCategories.push(newDeviceCategory);
          deviceCategoriesTable.renderRows();
        }
      )
    );
  }

  openEditDeviceCategoryModal(deviceCategoriesTable: MatTable<DeviceCategory>, id: number, deviceCategory: DeviceCategory) {
    const dialogRef = this.dialog.open(EditDeviceCategoryModalComponent,
      {
        width: EditDeviceCategoryModalComponent.MODAL_WIDTH,
        data: Object.assign({}, deviceCategory)
      }
    );
    dialogRef.afterClosed().subscribe((deviceCategoryAttrs: DeviceCategoryAttrs) =>
      this.deviceCategoryService.updateDeviceCategory(id, deviceCategoryAttrs).subscribe(
        (newDeviceCategory: DeviceCategory) => {
          deviceCategory.name = newDeviceCategory.name;
          deviceCategoriesTable.renderRows();
        }
      )
    );
  }

  deleteDeviceCategory(deviceCategoriesTable: MatTable<DeviceCategory>, id: number, deviceCategory: DeviceCategory) {
    this.deviceCategoryService.deleteDeviceCategory(id).subscribe(
      () => this.deleteDeviceCategoryFromDataSource(deviceCategoriesTable, deviceCategory)
    );
  }

  private deleteDeviceCategoryFromDataSource(deviceCategoriesTable: MatTable<DeviceCategory>, deviceCategory: DeviceCategory) {
    const index = this.deviceCategories.indexOf(deviceCategory, 0);
    if (index > -1) {
      this.deviceCategories.splice(index, 1);
      deviceCategoriesTable.renderRows();
    }
  }

}
