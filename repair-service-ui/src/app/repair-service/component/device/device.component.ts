import { Component, OnInit } from '@angular/core';
import { DeviceService } from '../../service/device.service';
import { Device } from '../../model/device';

@Component({
  selector: 'app-device',
  templateUrl: './device.component.html',
  styleUrls: ['./device.component.scss']
})
export class DeviceComponent implements OnInit {
  displayedColumns: string[] = ['id', 'deviceCategoryId', 'name', 'status'];
  devices: Device[];

  constructor(private deviceService: DeviceService) { }

  ngOnInit() {
    this.deviceService.getDevices().subscribe(
      (devices: Device[]) => this.devices = devices
    );
  }

}
