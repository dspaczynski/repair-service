import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

import { environment } from 'src/environments/environment';
import { throwError, Observable } from 'rxjs';
import { AuthService } from 'src/app/service/auth.service';
import { DeviceAttrs, Device } from '../model/device';
import { map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DeviceService {
  private endpoint: string;

  constructor(private http: HttpClient, private authService: AuthService) {
    this.endpoint = environment.apiUrl + '/device';
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occured', error.error.message);
    } else {
      console.error(
        `The backend returned an error: ${error.status}, ` +
        `message body: ${error.error}`);
    }
    return throwError(
      'Something went wrong...  ');
  }

  public getDevices(): Observable<Device[]> {
    return this.http.get<DeviceAttrs[]>(this.endpoint, this.authService.getBasicAuthTokenHeader()).pipe(
      map((data: DeviceAttrs[]) => data.map((deviceAttrs: DeviceAttrs) => new Device(deviceAttrs))),
      catchError(this.handleError)
    );
  }
}
