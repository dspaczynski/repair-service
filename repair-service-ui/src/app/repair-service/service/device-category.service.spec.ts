import { TestBed } from '@angular/core/testing';

import { DeviceCategoryService } from './device-category.service';

describe('DeviceCategoryService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DeviceCategoryService = TestBed.get(DeviceCategoryService);
    expect(service).toBeTruthy();
  });
});
