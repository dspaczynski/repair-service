import { Injectable } from '@angular/core';
import { AuthService } from 'src/app/service/auth.service';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { throwError, Observable } from 'rxjs';
import { DeviceCategory, DeviceCategoryAttrs } from '../model/device-category';
import { map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DeviceCategoryService {
  private endpoint: string;

  constructor(private http: HttpClient, private authService: AuthService) {
    this.endpoint = environment.apiUrl + '/device-category';
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occured', error.error.message);
    } else {
      console.error(
        `The backend returned an error: ${error.status}, ` +
        `message body: ${error.error}`);
    }
    return throwError(
      'Something went wrong...  ');
  }

  public getDeviceCategories(): Observable<DeviceCategory[]> {
    return this.http.get<DeviceCategoryAttrs[]>(this.endpoint, this.authService.getBasicAuthTokenHeader()).pipe(
      map((data: DeviceCategoryAttrs[]) => data.map((deviceAttrs: DeviceCategoryAttrs) => new DeviceCategory(deviceAttrs))),
      catchError(this.handleError)
    );
  }

  public getDeviceCategory(id: number): Observable<DeviceCategory> {
    return this.http.get<DeviceCategoryAttrs>(`${this.endpoint}/${id}`, this.authService.getBasicAuthTokenHeader()).pipe(
      map((data: DeviceCategoryAttrs) => new DeviceCategory(data)),
      catchError(this.handleError)
    );
  }

  public addDeviceCategory(deviceCategoryAttrs: DeviceCategoryAttrs): Observable<DeviceCategory> {
    return this.http.post<DeviceCategoryAttrs>(this.endpoint, deviceCategoryAttrs, this.authService.getBasicAuthTokenHeader()).pipe(
      map((data: DeviceCategoryAttrs) => new DeviceCategory(data)),
      catchError(this.handleError)
    );
  }

  public updateDeviceCategory(id: number, deviceCategoryAttrs: DeviceCategoryAttrs): Observable<DeviceCategory> {
    const url = `${this.endpoint}/${id}/name/${deviceCategoryAttrs.name}`;
    return this.http.patch<DeviceCategoryAttrs>(url, null, this.authService.getBasicAuthTokenHeader()).pipe(
      map((data: DeviceCategoryAttrs) => new DeviceCategory(data)),
      catchError(this.handleError)
    );
  }

  public deleteDeviceCategory(id: number): Observable<void> {
    return this.http.delete<void>(`${this.endpoint}/${id}`, this.authService.getBasicAuthTokenHeader());
  }
}
