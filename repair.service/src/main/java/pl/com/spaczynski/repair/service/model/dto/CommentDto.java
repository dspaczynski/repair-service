package pl.com.spaczynski.repair.service.model.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

public class CommentDto {
    private Long id;
    @NotBlank
    private String author;
    @NotBlank
    private String content;
    @NotNull
    private Long deviceId;
    @NotNull
    private Date date;

    public CommentDto() {
    }

    public CommentDto(Long id, @NotBlank String author, @NotBlank String content, @NotNull Long deviceId, @NotNull Date date) {
        this.id = id;
        this.author = author;
        this.content = content;
        this.deviceId = deviceId;
        this.date = date;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
