package pl.com.spaczynski.repair.service.model.entity;

import pl.com.spaczynski.repair.service.model.util.HibernateEntity;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Comment implements HibernateEntity {
    @Id
    @GeneratedValue
    private Long id;
    @Column
    private String author;
    @Column
    private String content;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DEVICE_ID")
    private Device device;
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date date;

    public Comment() {
    }

    public Comment(String author, String content, Device device, Date date) {
        this.author = author;
        this.content = content;
        this.device = device;
        this.date = date;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Device getDevice() {
        return device;
    }

    public void setDevice(Device device) {
        this.device = device;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
