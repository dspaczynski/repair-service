package pl.com.spaczynski.repair.service.service.implementation;

import org.springframework.stereotype.Service;
import pl.com.spaczynski.repair.service.model.dto.DeviceDto;
import pl.com.spaczynski.repair.service.model.entity.Device;
import pl.com.spaczynski.repair.service.model.entity.DeviceCategory;
import pl.com.spaczynski.repair.service.repository.DeviceRepository;
import pl.com.spaczynski.repair.service.service.DeviceCategoryService;
import pl.com.spaczynski.repair.service.service.DeviceService;
import pl.com.spaczynski.repair.service.util.mapper.DeviceMapper;

import java.util.List;
import java.util.NoSuchElementException;

@Service
public class DeviceServiceImplementation implements DeviceService {
    private static final String DEVICE_NOT_FOUND = "No device with id: \"%s\" has been found";

    private final DeviceMapper mapper;
    private final DeviceRepository repository;
    private final DeviceCategoryService deviceCategoryService;

    public DeviceServiceImplementation(final DeviceMapper mapper,
                                       final DeviceRepository repository,
                                       final DeviceCategoryService deviceCategoryService) {
        this.mapper = mapper;
        this.repository = repository;
        this.deviceCategoryService = deviceCategoryService;
    }

    @Override
    public DeviceDto updateDevice(final long id, final DeviceDto dto) {
        Device persistedDevice;

        try {
            persistedDevice = getDeviceById(id);
        } catch (NoSuchElementException ex) {
            return addDevice(dto);
        }

        return editDevice(persistedDevice, dto);
    }

    private DeviceDto editDevice(final Device persistedDevice, final DeviceDto dto) {
        final Long deviceCategoryId = dto.getDeviceCategoryId();
        final DeviceCategory deviceCategory = deviceCategoryService.getDeviceCategoryById(deviceCategoryId);

        final Device modifiedDevice = mapper.mapToDevice(dto);

        persistedDevice.setName(modifiedDevice.getName());
        persistedDevice.setDeviceCategory(deviceCategory);
        persistedDevice.setStatus(modifiedDevice.getStatus());
        persistedDevice.setParameters(modifiedDevice.getParameters());

        final Device updatedDevice = repository.save(persistedDevice);

        return mapper.mapToDeviceDto(updatedDevice);
    }

    @Override
    public DeviceDto addDevice(final DeviceDto dto) {
        final Long deviceCategoryId = dto.getDeviceCategoryId();
        final DeviceCategory deviceCategory = deviceCategoryService.getDeviceCategoryById(deviceCategoryId);

        final Device newDevice = mapper.mapToDevice(dto);
        newDevice.setDeviceCategory(deviceCategory);
        final Device persistedDevice = repository.save(newDevice);
        return mapper.mapToDeviceDto(persistedDevice);
    }

    @Override
    public Iterable<DeviceDto> getDevices() {
        final List<Device> devices = repository.findAll();
        return mapper.mapToDeviceDtos(devices);
    }

    @Override
    public DeviceDto getDevice(final long id) {
        final Device device = getDeviceById(id);
        return mapper.mapToDeviceDto(device);
    }

    @Override
    public void deleteDevice(final long id) {
        final Device device = getDeviceById(id);
        repository.delete(device);
    }

    @Override
    public Device getDeviceById(final long id) {
        return repository.find(id).orElseThrow(() ->
                new NoSuchElementException(String.format(DEVICE_NOT_FOUND, id))
        );
    }
}
