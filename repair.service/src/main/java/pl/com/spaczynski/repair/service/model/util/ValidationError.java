package pl.com.spaczynski.repair.service.model.util;

import org.springframework.validation.FieldError;

import java.util.HashMap;
import java.util.Map;

public class ValidationError extends RequestError {
    private static final String DEFAULT_MESSAGE = "Field validation error";
    private Map<String, String> fieldErrors = new HashMap<>();

    public ValidationError(int status, String message) {
        super(status, message);
    }

    public void addFieldError(final String path, final String message) {
        final FieldError fieldError = new FieldError(path, message, DEFAULT_MESSAGE);
        fieldErrors.put(fieldError.getObjectName(), fieldError.getField());
    }
}
