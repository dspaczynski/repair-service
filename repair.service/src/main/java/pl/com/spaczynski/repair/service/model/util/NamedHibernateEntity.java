package pl.com.spaczynski.repair.service.model.util;

public interface NamedHibernateEntity extends HibernateEntity {
    String getName();
    void setName(final String name);
}
