package pl.com.spaczynski.repair.service.repository;

import pl.com.spaczynski.repair.service.model.util.NamedHibernateEntity;

import java.util.Optional;

public interface NamedEntityRepository<T extends NamedHibernateEntity> extends Repository<T> {
    Optional<T> findByName(String name);
}
