package pl.com.spaczynski.repair.service.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.com.spaczynski.repair.service.model.dto.DeviceCategoryDto;
import pl.com.spaczynski.repair.service.service.DeviceCategoryService;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

@RestController
@RequestMapping(value = "/device-category")
public class DeviceCategoryController {
    private final DeviceCategoryService service;

    public DeviceCategoryController(final DeviceCategoryService service) {
        this.service = service;
    }

    @GetMapping
    public ResponseEntity<Iterable<DeviceCategoryDto>> getDeviceCategories() {
        final Iterable<DeviceCategoryDto> deviceCategoryDtos = service.getDeviceCategories();
        return new ResponseEntity<>(deviceCategoryDtos, HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<DeviceCategoryDto> getDeviceCategory(final @PathVariable long id) {
        final DeviceCategoryDto deviceCategoryDto = service.getDeviceCategory(id);
        return new ResponseEntity<>(deviceCategoryDto, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<DeviceCategoryDto> addDeviceCategory(final @Valid @RequestBody DeviceCategoryDto dto) {
        final DeviceCategoryDto addedDeviceCategoryDto = service.addDeviceCategory(dto);
        return new ResponseEntity<>(addedDeviceCategoryDto, HttpStatus.CREATED);
    }

    @PatchMapping(value = "/{id}/name/{newName}")
    public ResponseEntity<DeviceCategoryDto> changeCategoryName(final @PathVariable long id,
                                                                final @NotBlank @PathVariable String newName) {
        final DeviceCategoryDto modifiedDeviceCategoryDto = service.changeCategoryName(id, newName);
        return new ResponseEntity<>(modifiedDeviceCategoryDto, HttpStatus.ACCEPTED);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> deleteCategory(final @PathVariable long id) {
        service.deleteDeviceCategory(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
