package pl.com.spaczynski.repair.service.controller;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import pl.com.spaczynski.repair.service.model.util.RequestError;
import pl.com.spaczynski.repair.service.model.util.ValidationError;

import java.util.List;
import java.util.NoSuchElementException;

@RestControllerAdvice
public class ControllerAdvice extends ResponseEntityExceptionHandler {
    @ExceptionHandler(value = {IllegalArgumentException.class, IllegalStateException.class})
    protected ResponseEntity<Object> handleConflict(final Exception ex, final WebRequest request) {
        return handler(ex, request, HttpStatus.CONFLICT);
    }

    @ExceptionHandler(value = NoSuchElementException.class)
    protected ResponseEntity<Object> handleNotFound(final Exception ex, final WebRequest request) {
        return handler(ex, request, HttpStatus.NOT_FOUND);
    }

    private ResponseEntity<Object> handler(final Exception ex, final WebRequest request, final HttpStatus status) {
        final RequestError responseBody = new RequestError(status.value(), ex.getMessage());
        return handleExceptionInternal(ex, responseBody, new HttpHeaders(), status, request);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(final MethodArgumentNotValidException ex,
                                                                  final HttpHeaders headers,
                                                                  final HttpStatus status,
                                                                  final WebRequest request) {
        final BindingResult bindingResult = ex.getBindingResult();
        final List<FieldError> fieldErrors = bindingResult.getFieldErrors();
        final ValidationError responseBody = processFieldErrors(fieldErrors);
        return handleExceptionInternal(ex, responseBody, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    private ValidationError processFieldErrors(final List<FieldError> fieldErrors) {
        final ValidationError error = new ValidationError(HttpStatus.BAD_REQUEST.value(), "Validation error");
        fieldErrors.forEach(fieldError -> error.addFieldError(fieldError.getField(), fieldError.getDefaultMessage()));
        return error;
    }

}
