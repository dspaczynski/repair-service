package pl.com.spaczynski.repair.service.model.entity;

import org.hibernate.annotations.Type;
import pl.com.spaczynski.repair.service.model.util.DeviceParameters;
import pl.com.spaczynski.repair.service.model.util.DeviceStatus;
import pl.com.spaczynski.repair.service.model.util.NamedHibernateEntity;

import javax.persistence.*;
import java.util.List;

@Entity
public class Device implements NamedHibernateEntity {
    @Id
    @GeneratedValue
    private Long id;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CATEGORY_ID")
    private DeviceCategory deviceCategory;
    @Column(nullable = false)
    private String name;
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private DeviceStatus status;
    @Column
    @Type(type = "pl.com.spaczynski.repair.service.hibernate.JsonType")
    private DeviceParameters parameters;
    @OneToMany(mappedBy = "device", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Comment> comments;


    public Device() {
    }

    public Device(Long id, DeviceCategory deviceCategory, String name, DeviceStatus status, DeviceParameters parameters) {
        this.id = id;
        this.deviceCategory = deviceCategory;
        this.name = name;
        this.status = status;
        this.parameters = parameters;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public DeviceCategory getDeviceCategory() {
        return deviceCategory;
    }

    public void setDeviceCategory(DeviceCategory deviceCategory) {
        this.deviceCategory = deviceCategory;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DeviceStatus getStatus() {
        return status;
    }

    public void setStatus(DeviceStatus status) {
        this.status = status;
    }

    public DeviceParameters getParameters() {
        return parameters;
    }

    public void setParameters(DeviceParameters parameters) {
        this.parameters = parameters;
    }
}