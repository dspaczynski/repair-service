package pl.com.spaczynski.repair.service.model.entity;

import pl.com.spaczynski.repair.service.model.util.NamedHibernateEntity;

import javax.persistence.*;
import java.util.Set;

@Entity
public class DeviceCategory implements NamedHibernateEntity {
    @Id
    @GeneratedValue
    private Long id;
    @Column(nullable = false, unique = true)
    private String name;
    @OneToMany(mappedBy = "deviceCategory")
    private Set<Device> devices;

    public DeviceCategory() {
    }

    public DeviceCategory(String name, Set<Device> devices) {
        this.name = name;
        this.devices = devices;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Device> getDevices() {
        return devices;
    }

    public void setDevices(Set<Device> devices) {
        this.devices = devices;
    }
}