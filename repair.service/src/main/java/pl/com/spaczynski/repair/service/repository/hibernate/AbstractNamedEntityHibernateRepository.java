package pl.com.spaczynski.repair.service.repository.hibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import pl.com.spaczynski.repair.service.model.util.NamedHibernateEntity;
import pl.com.spaczynski.repair.service.repository.NamedEntityRepository;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.Optional;

public abstract class AbstractNamedEntityHibernateRepository<T extends NamedHibernateEntity> extends AbstractHibernateRepository<T> implements NamedEntityRepository<T> {
    public AbstractNamedEntityHibernateRepository(SessionFactory sessionFactory, Class<T> entityClass) {
        super(sessionFactory, entityClass);
    }

    @Override
    public Optional<T> findByName(String name) {
        final Session session = getCurrentSession();
        final CriteriaBuilder cb = session.getCriteriaBuilder();
        final CriteriaQuery<T> cq = cb.createQuery(getEntityClass());
        final Root<T> rootEntry = cq.from(getEntityClass());
        final Predicate nameEqualPredicate = cb.equal(rootEntry.get("name"), name);
        final CriteriaQuery<T> selectWhereName = cq.select(rootEntry).where(nameEqualPredicate);
        final TypedQuery<T> queryResult = session.createQuery(selectWhereName);
        return queryResult.getResultList().stream().findFirst();
    }
}
