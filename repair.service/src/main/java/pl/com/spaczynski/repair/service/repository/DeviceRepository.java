package pl.com.spaczynski.repair.service.repository;

import pl.com.spaczynski.repair.service.model.entity.Device;

public interface DeviceRepository extends NamedEntityRepository<Device> {
}
