package pl.com.spaczynski.repair.service.repository;

import pl.com.spaczynski.repair.service.model.util.HibernateEntity;

import java.util.List;
import java.util.Optional;

public interface Repository<T extends HibernateEntity> {
    Optional<T> find(final Long id);

    List<T> findAll();

    T save(final T entity);

    void delete(final T entity);

    void deleteById(final Long id);
}
