package pl.com.spaczynski.repair.service.model.util;

import java.io.Serializable;

public interface HibernateEntity extends Serializable {
    Long getId();

    void setId(final Long id);
}
