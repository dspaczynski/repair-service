package pl.com.spaczynski.repair.service.service.implementation;

import org.springframework.stereotype.Service;
import pl.com.spaczynski.repair.service.model.dto.DeviceCategoryDto;
import pl.com.spaczynski.repair.service.model.entity.DeviceCategory;
import pl.com.spaczynski.repair.service.repository.DeviceCategoryRepository;
import pl.com.spaczynski.repair.service.service.DeviceCategoryService;
import pl.com.spaczynski.repair.service.util.mapper.DeviceCategoryMapper;

import java.util.List;
import java.util.NoSuchElementException;

@Service
public class DeviceCategoryServiceImplementation implements DeviceCategoryService {
    private static final String NAME_IS_TAKEN = "A category with the name: \"%s\" already exists";
    private static final String CATEGORY_NOT_FOUND = "No category with id: \"%d\" has been found";
    private static final String NAME_IS_SAME = "The category has already name: \"%s\"";
    private static final String CATEGORY_IS_ASSIGNED = "The category is assigned to some device. Cannot be deleted";

    private final DeviceCategoryRepository repository;
    private final DeviceCategoryMapper mapper;

    public DeviceCategoryServiceImplementation(final DeviceCategoryRepository repository, final DeviceCategoryMapper mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    @Override
    public DeviceCategoryDto addDeviceCategory(final DeviceCategoryDto dto) {
        final String categoryName = dto.getName();
        throwExceptionWhenNameIsTaken(categoryName);
        final DeviceCategory newDeviceCategory = mapper.mapToDeviceCategory(dto);
        final DeviceCategory persistedDeviceCategory = repository.save(newDeviceCategory);
        return mapper.mapToDeviceCategoryDto(persistedDeviceCategory);
    }

    @Override
    public DeviceCategoryDto changeCategoryName(final long id, final String newName) {
        final DeviceCategory deviceCategoryToModify = getDeviceCategoryById(id);
        throwExceptionWhenNameIsNotChange(deviceCategoryToModify.getName(), newName);
        deviceCategoryToModify.setName(newName);
        final DeviceCategory modifiedDeviceCategory = repository.save(deviceCategoryToModify);
        return mapper.mapToDeviceCategoryDto(modifiedDeviceCategory);
    }

    private void throwExceptionWhenNameIsNotChange(final String oldName, final String newName) {
        if (oldName.equals(newName)) {
            throw new IllegalArgumentException(String.format(NAME_IS_SAME, oldName));
        }
        throwExceptionWhenNameIsTaken(newName);
    }

    private void throwExceptionWhenNameIsTaken(final String name) {
        if (isCategoryNameTaken(name)) {
            throw new IllegalArgumentException(String.format(NAME_IS_TAKEN, name));
        }
    }

    private boolean isCategoryNameTaken(final String name) {
        return repository.findByName(name).isPresent();
    }

    @Override
    public Iterable<DeviceCategoryDto> getDeviceCategories() {
        final List<DeviceCategory> deviceCategories = repository.findAll();
        return mapper.mapToDeviceCategoryDtos(deviceCategories);
    }

    @Override
    public DeviceCategoryDto getDeviceCategory(final long id) {
        final DeviceCategory deviceCategory = getDeviceCategoryById(id);
        return mapper.mapToDeviceCategoryDto(deviceCategory);
    }

    @Override
    public void deleteDeviceCategory(final long id) {
        final DeviceCategory deviceCategory = getDeviceCategoryById(id);
        if (!deviceCategory.getDevices().isEmpty()) {
            throw new IllegalStateException(CATEGORY_IS_ASSIGNED);
        }
        repository.delete(deviceCategory);
    }

    @Override
    public DeviceCategory getDeviceCategoryById(final long id) {
        return repository.find(id).orElseThrow(() ->
                new NoSuchElementException(String.format(CATEGORY_NOT_FOUND, id))
        );
    }
}
