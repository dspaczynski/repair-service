package pl.com.spaczynski.repair.service.model.dto;

import javax.validation.constraints.NotBlank;

public class DeviceCategoryDto {
    private Long id;
    @NotBlank
    private String name;

    public DeviceCategoryDto() {
    }

    public DeviceCategoryDto(final Long id, final @NotBlank String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }
}

