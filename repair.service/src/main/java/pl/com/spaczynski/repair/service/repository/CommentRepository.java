package pl.com.spaczynski.repair.service.repository;

import pl.com.spaczynski.repair.service.model.entity.Comment;

import java.util.List;

public interface CommentRepository extends Repository<Comment> {
    List<Comment> findAllByDeviceId(final long id);
}
