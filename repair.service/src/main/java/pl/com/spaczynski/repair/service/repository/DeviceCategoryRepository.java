package pl.com.spaczynski.repair.service.repository;

import pl.com.spaczynski.repair.service.model.entity.DeviceCategory;

public interface DeviceCategoryRepository extends NamedEntityRepository<DeviceCategory> {
}
