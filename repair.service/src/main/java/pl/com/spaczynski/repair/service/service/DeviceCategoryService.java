package pl.com.spaczynski.repair.service.service;

import pl.com.spaczynski.repair.service.model.dto.DeviceCategoryDto;
import pl.com.spaczynski.repair.service.model.entity.DeviceCategory;

public interface DeviceCategoryService {
    DeviceCategoryDto addDeviceCategory(DeviceCategoryDto dto);

    DeviceCategoryDto changeCategoryName(long id, String newName);

    Iterable<DeviceCategoryDto> getDeviceCategories();

    DeviceCategoryDto getDeviceCategory(long id);

    DeviceCategory getDeviceCategoryById(final long id);

    void deleteDeviceCategory(long id);
}
