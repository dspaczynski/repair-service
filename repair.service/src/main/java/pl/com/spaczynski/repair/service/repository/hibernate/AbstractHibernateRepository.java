package pl.com.spaczynski.repair.service.repository.hibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import pl.com.spaczynski.repair.service.model.util.HibernateEntity;
import pl.com.spaczynski.repair.service.repository.Repository;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Optional;

public abstract class AbstractHibernateRepository<T extends HibernateEntity> implements Repository<T> {
    private static final String NULL_ENTITY = "The provided entity of type: %s equals NULL";

    private final SessionFactory sessionFactory;
    private final Class<T> entityClass;

    public AbstractHibernateRepository(final SessionFactory sessionFactory, final Class<T> entityClass) {
        this.sessionFactory = sessionFactory;
        this.entityClass = entityClass;
    }

    @Override
    public Optional<T> find(final Long id) {
        final Session session = getCurrentSession();
        final T entity = session.get(entityClass, id);
        if (null == entity) {
            return Optional.empty();
        }
        return Optional.of(entity);
    }

    @Override
    public List<T> findAll() {
        final Session session = getCurrentSession();
        final CriteriaBuilder cb = session.getCriteriaBuilder();
        final CriteriaQuery<T> cq = cb.createQuery(entityClass);
        final Root<T> rootEntry = cq.from(entityClass);
        final CriteriaQuery<T> selectAll = cq.select(rootEntry);
        final TypedQuery<T> queryResult = session.createQuery(selectAll);
        return queryResult.getResultList();
    }

    @Override
    public T save(final T entity) {
        if (null == entity) {
            throw new IllegalArgumentException(String.format(NULL_ENTITY, entityClass.getName()));
        }
        final Long entityId = entity.getId();
        if (null != entityId && find(entityId).isPresent()) {
            return updatePersisted(entity);
        }
        return saveTransient(entity);
    }

    private T saveTransient(final T entity) {
        final Session session = getCurrentSession();
        session.beginTransaction();
        final Long id = (Long) session.save(entity);
        session.getTransaction().commit();
        entity.setId(id);
        return entity;
    }

    private T updatePersisted(final T entity) {
        final Session session = getCurrentSession();
        session.beginTransaction();
        session.update(entity);
        session.getTransaction().commit();
        return entity;
    }

    @Override
    public void delete(final T entity) {
        final Session session = getCurrentSession();
        session.beginTransaction();
        session.delete(entity);
        session.getTransaction().commit();
    }

    @Override
    public void deleteById(final Long id) {
        final Optional<T> optional = find(id);
        optional.ifPresent(this::delete);
    }

    protected Session getCurrentSession() {
        return this.sessionFactory.getCurrentSession();
    }

    protected Class<T> getEntityClass() {
        return entityClass;
    }
}
