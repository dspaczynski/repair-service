package pl.com.spaczynski.repair.service.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.com.spaczynski.repair.service.model.dto.CommentDto;
import pl.com.spaczynski.repair.service.service.CommentService;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/comments")
public class CommentController {
    private final CommentService commentService;

    public CommentController(CommentService commentService) {
        this.commentService = commentService;
    }

    @PostMapping
    public ResponseEntity<CommentDto> addComment(final @Valid @RequestBody CommentDto dto) {
        final CommentDto addedCommentDto = commentService.addComment(dto);
        return new ResponseEntity<>(addedCommentDto, HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<Iterable<CommentDto>> getComments(final @RequestParam(required = false) Long deviceId) {
        Iterable<CommentDto> commentDtos;
        if (null == deviceId) {
            commentDtos = commentService.getComments();
        } else {
            commentDtos = commentService.getCommentsByDeviceId(deviceId);
        }

        return new ResponseEntity<>(commentDtos, HttpStatus.OK);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<CommentDto> updateComment(final @PathVariable long id, final @Valid @RequestBody CommentDto dto) {
        final CommentDto updatedComment = commentService.updateComment(id, dto);
        return new ResponseEntity<>(updatedComment, HttpStatus.ACCEPTED);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> deleteComment(final @PathVariable long id) {
        commentService.deleteComment(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
