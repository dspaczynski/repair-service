package pl.com.spaczynski.repair.service.model.dto;

import pl.com.spaczynski.repair.service.model.util.DeviceStatus;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Map;

public class DeviceDto {
    private Long id;
    @NotNull
    private Long deviceCategoryId;
    @NotBlank
    private String name;
    @NotNull
    private DeviceStatus status;
    private Map<String, String> parameters;

    public DeviceDto() {
    }

    public DeviceDto(Long id, @NotNull Long deviceCategoryId, @NotBlank String name, @NotNull DeviceStatus status, Map<String, String> parameters) {
        this.id = id;
        this.deviceCategoryId = deviceCategoryId;
        this.name = name;
        this.status = status;
        this.parameters = parameters;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getDeviceCategoryId() {
        return deviceCategoryId;
    }

    public void setDeviceCategoryId(Long deviceCategoryId) {
        this.deviceCategoryId = deviceCategoryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DeviceStatus getStatus() {
        return status;
    }

    public void setStatus(DeviceStatus status) {
        this.status = status;
    }

    public Map<String, String> getParameters() {
        return parameters;
    }

    public void setParameters(Map<String, String> parameters) {
        this.parameters = parameters;
    }
}
