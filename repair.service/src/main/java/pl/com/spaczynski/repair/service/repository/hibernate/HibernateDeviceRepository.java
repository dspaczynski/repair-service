package pl.com.spaczynski.repair.service.repository.hibernate;

import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import pl.com.spaczynski.repair.service.model.entity.Device;
import pl.com.spaczynski.repair.service.repository.DeviceRepository;

@Repository
public class HibernateDeviceRepository extends AbstractNamedEntityHibernateRepository<Device> implements DeviceRepository {
    public HibernateDeviceRepository(SessionFactory sessionFactory) {
        super(sessionFactory, Device.class);
    }
}
