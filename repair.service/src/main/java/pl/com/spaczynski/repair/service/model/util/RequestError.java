package pl.com.spaczynski.repair.service.model.util;

public class RequestError {
    protected final int status;
    protected final String message;

    public RequestError(int status, String message) {
        this.status = status;
        this.message = message;
    }

    public int getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }
}
