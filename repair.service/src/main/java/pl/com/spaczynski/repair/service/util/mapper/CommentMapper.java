package pl.com.spaczynski.repair.service.util.mapper;


import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import pl.com.spaczynski.repair.service.model.dto.CommentDto;
import pl.com.spaczynski.repair.service.model.entity.Comment;

import java.util.List;

@Mapper(componentModel = "spring")
public abstract class CommentMapper {
    @Mapping(target = "device", ignore = true)
    public abstract Comment mapToComment(final CommentDto dto);

    public abstract List<CommentDto> mapToCommentDtos(final List<Comment> comments);

    @Mapping(target = "deviceId", ignore = true)
    public abstract CommentDto mapToCommentDto(final Comment comment);

    @AfterMapping
    public void setDeviceID(final Comment comment, final @MappingTarget CommentDto dto) {
        final Long deviceId = comment.getDevice().getId();
        dto.setDeviceId(deviceId);
    }
}
