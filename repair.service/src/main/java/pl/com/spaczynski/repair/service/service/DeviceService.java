package pl.com.spaczynski.repair.service.service;

import pl.com.spaczynski.repair.service.model.dto.DeviceDto;
import pl.com.spaczynski.repair.service.model.entity.Device;

public interface DeviceService {
    DeviceDto addDevice(final DeviceDto dto);

    DeviceDto updateDevice(final long id, final DeviceDto dto);

    Iterable<DeviceDto> getDevices();

    DeviceDto getDevice(final long id);

    void deleteDevice(final long id);

    Device getDeviceById(final long id);
}
