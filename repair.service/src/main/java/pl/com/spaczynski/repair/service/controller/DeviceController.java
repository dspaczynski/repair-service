package pl.com.spaczynski.repair.service.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.com.spaczynski.repair.service.model.dto.DeviceDto;
import pl.com.spaczynski.repair.service.service.DeviceService;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/device")
public class DeviceController {
    private final DeviceService deviceService;

    public DeviceController(DeviceService deviceService) {
        this.deviceService = deviceService;
    }

    @PostMapping
    public ResponseEntity<DeviceDto> addDevice(final @RequestBody @Valid DeviceDto dto) {
        final DeviceDto newDeviceDto = deviceService.addDevice(dto);
        return new ResponseEntity<>(newDeviceDto, HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<Iterable<DeviceDto>> getDevices() {
        final Iterable<DeviceDto> deviceDtos = deviceService.getDevices();
        return new ResponseEntity<>(deviceDtos, HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<DeviceDto> getDevice(final @PathVariable long id) {
        final DeviceDto deviceDto = deviceService.getDevice(id);
        return new ResponseEntity<>(deviceDto, HttpStatus.OK);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<DeviceDto> updateDevice(final @PathVariable long id, final @Valid @RequestBody DeviceDto dto) {
        final DeviceDto deviceDto = deviceService.updateDevice(id, dto);
        return new ResponseEntity<>(deviceDto, HttpStatus.ACCEPTED);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> deleteDevice(final @PathVariable long id) {
        deviceService.deleteDevice(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
