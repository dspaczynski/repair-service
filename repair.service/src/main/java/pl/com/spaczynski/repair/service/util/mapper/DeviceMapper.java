package pl.com.spaczynski.repair.service.util.mapper;

import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import pl.com.spaczynski.repair.service.model.dto.DeviceDto;
import pl.com.spaczynski.repair.service.model.entity.Device;
import pl.com.spaczynski.repair.service.model.util.DeviceParameters;

import java.util.List;
import java.util.Map;

@Mapper(componentModel = "spring")
public abstract class DeviceMapper {
    @Mapping(target = "deviceCategory", ignore = true)
    @Mapping(target = "parameters", ignore = true)
    public abstract Device mapToDevice(final DeviceDto dto);

    @AfterMapping
    public void setDeviceParameters(final DeviceDto dto, final @MappingTarget Device device) {
        final Map<String, String> parameters = dto.getParameters();
        final DeviceParameters deviceParameters = new DeviceParameters();
        deviceParameters.setParameters(parameters);
        device.setParameters(deviceParameters);
    }

    public abstract List<DeviceDto> mapToDeviceDtos(final List<Device> devices);

    @Mapping(target = "deviceCategoryId", ignore = true)
    @Mapping(target = "parameters", ignore = true)
    public abstract DeviceDto mapToDeviceDto(final Device device);

    @AfterMapping
    public void setDeviceCategoryId(final Device device, final @MappingTarget DeviceDto dto) {
        final Long deviceCategoryId = device.getDeviceCategory().getId();
        dto.setDeviceCategoryId(deviceCategoryId);
    }

    @AfterMapping
    public void setParameters(final Device device, final @MappingTarget DeviceDto dto) {
        final Map<String, String> parameters = device.getParameters().getParameters();
        dto.setParameters(parameters);
    }
}
