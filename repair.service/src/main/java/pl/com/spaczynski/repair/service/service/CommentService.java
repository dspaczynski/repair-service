package pl.com.spaczynski.repair.service.service;

import pl.com.spaczynski.repair.service.model.dto.CommentDto;

public interface CommentService {
    CommentDto addComment(CommentDto dto);

    Iterable<CommentDto> getComments();

    Iterable<CommentDto> getCommentsByDeviceId(long deviceId);

    CommentDto updateComment(long id, CommentDto dto);

    void deleteComment(long id);
}
