package pl.com.spaczynski.repair.service.repository.hibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import pl.com.spaczynski.repair.service.model.entity.Comment;
import pl.com.spaczynski.repair.service.repository.CommentRepository;

import java.util.List;

@Repository
public class HibernateCommentRepository extends AbstractHibernateRepository<Comment> implements CommentRepository {
    public HibernateCommentRepository(SessionFactory sessionFactory) {
        super(sessionFactory, Comment.class);
    }

    public List<Comment> findAllByDeviceId(final long id) {
        final String hql = "FROM Comment C WHERE C.device.id = :device_id";
        final Session session = getCurrentSession();
        final Query<Comment> query = session.createQuery(hql, Comment.class);
        query.setParameter("device_id", id);
        return query.getResultList();
    }
}
