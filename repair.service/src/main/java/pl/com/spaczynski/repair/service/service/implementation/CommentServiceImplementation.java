package pl.com.spaczynski.repair.service.service.implementation;

import org.springframework.stereotype.Service;
import pl.com.spaczynski.repair.service.model.dto.CommentDto;
import pl.com.spaczynski.repair.service.model.entity.Comment;
import pl.com.spaczynski.repair.service.model.entity.Device;
import pl.com.spaczynski.repair.service.repository.CommentRepository;
import pl.com.spaczynski.repair.service.service.DeviceService;
import pl.com.spaczynski.repair.service.util.mapper.CommentMapper;

import java.util.List;
import java.util.NoSuchElementException;

@Service
public class CommentServiceImplementation implements pl.com.spaczynski.repair.service.service.CommentService {
    private static final String COMMENT_NOT_FOUND = "No comment with id: \"%s\" has been found";

    private final CommentRepository repository;
    private final CommentMapper mapper;
    private final DeviceService deviceService;

    public CommentServiceImplementation(CommentRepository repository, CommentMapper mapper, DeviceService deviceService) {
        this.repository = repository;
        this.mapper = mapper;
        this.deviceService = deviceService;
    }

    @Override
    public CommentDto addComment(final CommentDto dto) {
        final Long deviceId = dto.getDeviceId();
        final Device device = deviceService.getDeviceById(deviceId);

        final Comment newComment = mapper.mapToComment(dto);
        newComment.setDevice(device);

        final Comment persistedComment = repository.save(newComment);
        return mapper.mapToCommentDto(persistedComment);
    }

    @Override
    public Iterable<CommentDto> getComments() {
        final List<Comment> comments = repository.findAll();
        return mapper.mapToCommentDtos(comments);
    }

    @Override
    public Iterable<CommentDto> getCommentsByDeviceId(final long deviceId) {
        final List<Comment> comments = repository.findAllByDeviceId(deviceId);
        return mapper.mapToCommentDtos(comments);
    }

    @Override
    public CommentDto updateComment(final long id, final CommentDto dto) {
        Comment persistedComment;

        try {
            persistedComment = getCommentById(id);
        } catch (NoSuchElementException ex) {
            return addComment(dto);
        }

        return editComment(persistedComment, dto);
    }

    private CommentDto editComment(final Comment persistedComment, final CommentDto dto) {
        final Long deviceId = dto.getDeviceId();
        final Device device = deviceService.getDeviceById(deviceId);

        final Comment modifiedComment = mapper.mapToComment(dto);

        persistedComment.setAuthor(modifiedComment.getAuthor());
        persistedComment.setDevice(device);
        persistedComment.setContent(modifiedComment.getContent());
        persistedComment.setDate(modifiedComment.getDate());

        final Comment updatedComment = repository.save(persistedComment);

        return mapper.mapToCommentDto(updatedComment);
    }

    @Override
    public void deleteComment(final long id) {
        final Comment comment = getCommentById(id);
        repository.delete(comment);
    }

    private Comment getCommentById(final long id) {
        return repository.find(id).orElseThrow(() ->
                new NoSuchElementException(String.format(COMMENT_NOT_FOUND, id))
        );
    }
}
