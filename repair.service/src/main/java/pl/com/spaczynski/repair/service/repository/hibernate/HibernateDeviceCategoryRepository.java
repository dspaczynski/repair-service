package pl.com.spaczynski.repair.service.repository.hibernate;

import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import pl.com.spaczynski.repair.service.model.entity.DeviceCategory;
import pl.com.spaczynski.repair.service.repository.DeviceCategoryRepository;

@Repository
public class HibernateDeviceCategoryRepository extends AbstractNamedEntityHibernateRepository<DeviceCategory> implements DeviceCategoryRepository {
    public HibernateDeviceCategoryRepository(final SessionFactory sessionFactory) {
        super(sessionFactory, DeviceCategory.class);
    }
}
