package pl.com.spaczynski.repair.service.util.mapper;

import org.mapstruct.Mapper;
import pl.com.spaczynski.repair.service.model.dto.DeviceCategoryDto;
import pl.com.spaczynski.repair.service.model.entity.DeviceCategory;

import java.util.List;

@Mapper(componentModel = "spring")
public interface DeviceCategoryMapper {
    DeviceCategory mapToDeviceCategory(DeviceCategoryDto dto);

    DeviceCategoryDto mapToDeviceCategoryDto(DeviceCategory category);

    List<DeviceCategoryDto> mapToDeviceCategoryDtos(List<DeviceCategory> deviceCategories);
}

