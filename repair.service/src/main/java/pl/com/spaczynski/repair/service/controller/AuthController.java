package pl.com.spaczynski.repair.service.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/auth")
public class AuthController {
    @RequestMapping
    public ResponseEntity<Boolean> checkBasicAuthorization() {
        return new ResponseEntity<>(new Boolean(true), HttpStatus.OK);
    }
}
