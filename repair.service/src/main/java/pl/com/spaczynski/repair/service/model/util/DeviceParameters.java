package pl.com.spaczynski.repair.service.model.util;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class DeviceParameters implements Serializable {
    private Map<String, String> parameters = new HashMap<>();

    public void setParameters(Map<String, String> parameters) {
        this.parameters = parameters;
    }

    public Map<String, String> getParameters() {
        return this.parameters;
    }
}
